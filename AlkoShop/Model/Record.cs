﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Serialization;

namespace WCFServiceAlkoShop
{
    [DataContract]
    public class Record
    {
        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public string Name { set; get; }

        [DataMember]
        public int Price { set; get; }

        [DataMember]
        public DateTime Dt { set; get; }
    }
}