﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFServiceAlkoShop
{
    [ServiceContract]
    public interface IShopService
    {
        [OperationContract]
        List<Record> GetAllRecords();

        [OperationContract]
        void AddRecord(Record record);

        [OperationContract]
        void DeleteRecord(int id);

        [OperationContract]
        void UpdateRecord(Record record);
    }
}
