﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MySql.Data.MySqlClient;

namespace WCFServiceAlkoShop
{
    public class TableRecords
    {
        private MySqlCommand sqlCommand;

        public TableRecords()
        {
            string connectionString = "Server=localhost;User=root;Password=1234;Database=alko_shop";

            MySqlConnection connection = new MySqlConnection(connectionString);

            connection.Open();

            sqlCommand = new MySqlCommand();
            sqlCommand.Connection = connection;
        }

        public List<Record> SelectAll()
        {
            List<Record> records = new List<Record>();

            sqlCommand.CommandText = "SELECT * FROM `records`";
            MySqlDataReader dataReader = sqlCommand.ExecuteReader();

            while(dataReader.Read())
            {
                records.Add(new Record
                {
                    Id = dataReader.GetInt32("id"),
                    Name = dataReader.GetString("name"),
                    Price = dataReader.GetInt32("price"),
                    Dt = dataReader.GetDateTime("dt")
                });
            }
            dataReader.Close();

            return records;
        }

        public void Add(Record record)
        {
            sqlCommand.CommandText = $@"INSERT INTO `records`
                (`id`,`name`,`price`,`dt`)
                VALUES(
                {record.Id},
                '{record.Name}',
                '{record.Price}',
                '{record.Dt.ToString("yyyy-MM-dd H:mm:ss")}'
                )";
            sqlCommand.ExecuteNonQuery();            
        }

        public void Delete(int id)
        {
            sqlCommand.CommandText = $@"DELETE FROM `records` WHERE `id` = {id}";
            sqlCommand.ExecuteNonQuery();
        }

        public void Update(Record record)
        {
            sqlCommand.CommandText = $@"UPDATE `records` SET 
            `name` = '{record.Name}',
            `price` = '{record.Price}',
            `dt` = '{record.Dt.ToString("yyyy-MM-dd H:mm:ss")}'
            WHERE `id` = {record.Id}";

            sqlCommand.ExecuteNonQuery();
        }
    }
}