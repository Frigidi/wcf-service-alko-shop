﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFServiceAlkoShop
{
    public class ShopService : IShopService
    {
        private TableRecords tableRecords;

        public ShopService()
        {
            tableRecords = new TableRecords();
        }

        public List<Record> GetAllRecords()
        {
            return tableRecords.SelectAll();
        }

        public void AddRecord(Record record)
        {
            tableRecords.Add(record);
        }

        public void DeleteRecord(int id)
        {
            tableRecords.Delete(id);
        }

        public void UpdateRecord(Record record)
        {
            tableRecords.Update(record);
        }
    }
}
